package com.capgemini.mrchecker.tests.ftthInstallation.utils;

public class DataProviderInternal {
	private String projectName;

	
	public String getProjectName() {
		return projectName;
	}
	
	public DataProviderInternal setProjectName(String projectName) {
		this.projectName = projectName;
		return this;
	}
	
	
	@Override
	public String toString() {
		return "projectName ((" + getProjectName() + ")";
	}
}