package com.selenium.tests.availability;

import static org.junit.Assert.*;

import org.junit.Test;

import com.capgemini.mrchecker.selenium.core.newDrivers.elementType.ListElements;
import com.capgemini.mrchecker.test.core.BaseTest;
import com.capgemini.mrchecker.test.core.logger.BFLogger;
import com.capgemini.mrchecker.tests.ftthInstallation.utils.DataProviderInternal;
import com.selenium.pages.tmagic.AvailabilityPage;
import com.selenium.pages.tmagic.MainPage;
import com.selenium.pages.tmagic.availabilitycheck.AvailabilityTestPage;
import com.selenium.pages.tmagic.ftthInstallation.FtthSearchPage;


import junitparams.Parameters;


public class AvailabilityTest extends BaseTest {

	private Object[] data() {
		return new Object[] {
				new DataProviderInternal()
						.setProjectName("Project-Name_3"),
				 //next data
						
				new DataProviderInternal()
						.setProjectName("Project-Name_4")
						};
	}
	
	@Test
	public void searchAvailabilityPositiveResult() {
		String positiveKlsId = "1000001";
		String plannedEndDate = "Mon Dec 31 00:00:00 UTC 2018";
		
		BFLogger.logInfo("[Step 1] Open main page");
		AvailabilityPage mainPage = new AvailabilityPage();
		
		BFLogger.logInfo("[Step 2] Click navigation bar - Availability Test Page");
		AvailabilityTestPage availabilityTestPage = mainPage.clickNavBarAvailabilityTestPage();
		
		BFLogger.logInfo("[Step 3] Enter kls ID");
		availabilityTestPage.setKlsId(positiveKlsId);

		BFLogger.logInfo("[Step 4] Click Search");
		availabilityTestPage.clickSearch();

		BFLogger.logInfo("[Step 5] Verify if status is 'PLANNED'");
		assertTrue("status is NOT 'PLANNED'.", availabilityTestPage.getStatusOutput().equals("PLANNED"));

		BFLogger.logInfo("[Step 6] Verify if klsId is " + positiveKlsId);
		assertTrue("kls IDs don't match.", availabilityTestPage.getKlsIdOutput().equals(positiveKlsId));

		BFLogger.logInfo("[Step 7] Verify if planned installation start is empty");
		assertTrue("planned installation start is NOT empty.", availabilityTestPage.getPlannedStartOutput().isEmpty());

		BFLogger.logInfo("[Step 8] Verify if planned installation end is '" + plannedEndDate + "'");
		assertTrue("planned installation end is NOT '" + plannedEndDate + "'.", availabilityTestPage.getPlannedEndOutput().equals(plannedEndDate));

		BFLogger.logInfo("[Step 9] Verify if reason for no construction is empty");
		assertTrue("reason for no construction is NOT empty.", availabilityTestPage.getReasonForNoConstructionOutput().isEmpty());

		BFLogger.logInfo("[Step 10] Verify if termination error is empty");
		assertTrue("termination error is NOT empty.", availabilityTestPage.getTerminationErrorOutput().isEmpty());
	}
	
	@Test
	public void searchAvailabilityNotPlannedResult() {
		String notPlannedKlsId = "1000010";
		String plannedStartDate = "Sun Jul 01 00:00:00 UTC 2018";
		String plannedEndDate = "Sun Sep 30 00:00:00 UTC 2018";
		String noOwnerCommitText = "no fiber expansion planned, because we have no owner commit";
		
		BFLogger.logInfo("[Step 1] Open main page");
		AvailabilityPage mainPage = new AvailabilityPage();
		
		BFLogger.logInfo("[Step 2] Click navigation bar - Availability Test Page");
		AvailabilityTestPage availabilityTestPage = mainPage.clickNavBarAvailabilityTestPage();
		
		BFLogger.logInfo("[Step 3] Enter kls ID");
		availabilityTestPage.setKlsId(notPlannedKlsId);

		BFLogger.logInfo("[Step 4] Click Search");
		availabilityTestPage.clickSearch();

		BFLogger.logInfo("[Step 5] Verify if status is 'NOT_PLANNED'");
		assertTrue("status is NOT 'NOT_PLANNED'.", availabilityTestPage.getStatusOutput().equals("NOT_PLANNED"));

		BFLogger.logInfo("[Step 6] Verify if klsId is " + notPlannedKlsId);
		assertTrue("kls IDs don't match.", availabilityTestPage.getKlsIdOutput().equals(notPlannedKlsId));

		BFLogger.logInfo("[Step 7] Verify if planned installation start is " + plannedStartDate);
		assertTrue("planned installation start is NOT " + plannedStartDate + ".", availabilityTestPage.getPlannedStartOutput().equals(plannedStartDate));

		BFLogger.logInfo("[Step 8] Verify if planned installation end is " + plannedEndDate);
		assertTrue("planned installation end is NOT " + plannedEndDate + ".", availabilityTestPage.getPlannedEndOutput().equals(plannedEndDate));

		BFLogger.logInfo("[Step 9] Verify if reason for no construction is '" + noOwnerCommitText + "'");
		assertTrue("reason for no construction is NOT '" + noOwnerCommitText + "'.", availabilityTestPage.getReasonForNoConstructionOutput().equals(noOwnerCommitText));

		BFLogger.logInfo("[Step 10] Verify if termination error is empty");
		assertTrue("termination error is NOT empty.", availabilityTestPage.getTerminationErrorOutput().isEmpty());
	}
	
	@Test
	public void searchAvailabilityNoResult() {
		String notPlannedKlsId = "1000000";
		String terminationErrorText = "KLS-ID nicht gefunden";
		
		BFLogger.logInfo("[Step 1] Open main page");
		AvailabilityPage mainPage = new AvailabilityPage();
		
		BFLogger.logInfo("[Step 2] Click navigation bar - Availability Test Page");
		AvailabilityTestPage availabilityTestPage = mainPage.clickNavBarAvailabilityTestPage();
		
		BFLogger.logInfo("[Step 3] Enter kls ID");
		availabilityTestPage.setKlsId(notPlannedKlsId);

		BFLogger.logInfo("[Step 4] Click Search");
		availabilityTestPage.clickSearch();

		BFLogger.logInfo("[Step 5] Verify if status is empty");
		assertTrue("status is NOT empty.", availabilityTestPage.getStatusOutput().isEmpty());

		BFLogger.logInfo("[Step 6] Verify if klsId is " + notPlannedKlsId);
		assertTrue("kls IDs don't match.", availabilityTestPage.getKlsIdOutput().equals(notPlannedKlsId));

		BFLogger.logInfo("[Step 7] Verify if planned installation start is empty");
		assertTrue("planned installation start is NOT empty.", availabilityTestPage.getPlannedStartOutput().isEmpty());

		BFLogger.logInfo("[Step 8] Verify if planned installation end is empty");
		assertTrue("planned installation end is NOT empty.", availabilityTestPage.getPlannedEndOutput().isEmpty());

		BFLogger.logInfo("[Step 9] Verify if reason for no construction is empty");
		assertTrue("reason for no construction is NOT empty.", availabilityTestPage.getReasonForNoConstructionOutput().isEmpty());

		BFLogger.logInfo("[Step 10] Verify if termination error is '" + terminationErrorText + "'");
		assertTrue("termination error is NOT '" + terminationErrorText + "'.", availabilityTestPage.getTerminationErrorOutput().equals(terminationErrorText));
	}
	
		
	@Override
	public void setUp() {
		// TODO Auto-generated method stub
		
	}
	
	@Override
	public void tearDown() {
		// TODO Auto-generated method stub
		
	}
}
