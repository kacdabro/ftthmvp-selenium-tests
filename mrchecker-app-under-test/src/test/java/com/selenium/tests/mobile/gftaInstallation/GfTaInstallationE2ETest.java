package com.selenium.tests.mobile.gftaInstallation;

import static org.junit.Assert.assertTrue;

import org.junit.Test;

import com.capgemini.mrchecker.selenium.core.newDrivers.elementType.ListElements;
import com.capgemini.mrchecker.test.core.BaseTest;
import com.capgemini.mrchecker.test.core.logger.BFLogger;
import com.capgemini.mrchecker.tests.ftthInstallation.utils.DataProviderInternal;
import com.selenium.pages.tmagic.MainPage;
import com.selenium.pages.tmagic.ftthInstallation.FtthSearchPage;
import com.selenium.pages.tmagic.mobile.gfTaInventarisation.GfTaMontagePage;
import com.selenium.pages.tmagic.mobile.gfTaInventarisation.WorkorderSelectionPage;

import junitparams.Parameters;

public class GfTaInstallationE2ETest extends BaseTest {

	private Object[] data() {
		return new Object[] {
				new DataProviderInternal()
						.setProjectName("Project-Name_3"),
				 //next data
				new DataProviderInternal()
						.setProjectName("Project-Name_4")
						};
	}
	
	@Test
	public void installationOfGfTaInMobileApp() {
		
		BFLogger.logInfo("[Step 1] Open mobile Client");
		WorkorderSelectionPage workorderSelectionPage = new WorkorderSelectionPage(null, null);
		
		BFLogger.logInfo("[Step 2] Click on first Workorder - get streetname");
		workorderSelectionPage.clickFirstWorkorder();
		String streetName = workorderSelectionPage.getGfTaStreet();
		
		BFLogger.logInfo("[Step 3] Click on modify Workorder");
		GfTaMontagePage gfTaMontage = workorderSelectionPage.clickModifyWorkorder();
		
		BFLogger.logInfo("[Step 4] Check street name and select GF-TA");

	}
		
		
	@Override
	public void setUp() {
		// TODO Auto-generated method stub
		
	}
	
	@Override
	public void tearDown() {
		// TODO Auto-generated method stub
		
	}
}
