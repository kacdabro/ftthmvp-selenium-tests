package com.selenium.tests.ftthInstallation;

import static org.junit.Assert.*;

import org.junit.Test;

import com.capgemini.mrchecker.selenium.core.newDrivers.elementType.ListElements;
import com.capgemini.mrchecker.test.core.BaseTest;
import com.capgemini.mrchecker.test.core.logger.BFLogger;
import com.capgemini.mrchecker.tests.ftthInstallation.utils.DataProviderInternal;
import com.selenium.pages.tmagic.MainPage;
import com.selenium.pages.tmagic.ftthInstallation.FtthSearchPage;


import junitparams.Parameters;


public class FtthProjectSearchTest extends BaseTest {

	private Object[] data() {
		return new Object[] {
				new DataProviderInternal()
						.setProjectName("Project-Name_3"),
				 //next data
						
				new DataProviderInternal()
						.setProjectName("Project-Name_4")
						};
	}
	
	@Test
	public void searchFtthProjectWithoutSearchCriteria() {
		
		BFLogger.logInfo("[Step 1] Open main page");
		MainPage mainPage = new MainPage();
		
		BFLogger.logInfo("[Step 2] Click navigation bar - FTTH Installationsprojekt Suchen");
		mainPage.clickNavBarFtthInstallation();
		FtthSearchPage ftthSearch = mainPage.clickNavBarFtthProjectSuchen();
		
		BFLogger.logInfo("[Step 3] Click Search");
		ftthSearch.clickSearch();

		BFLogger.logInfo("[Step 4] Verify if search results are loaded");
		ListElements results = ftthSearch.getResultList();
		assertTrue("No search results shown.", results.getSize() > 0);
	}
	
	@Test
	@Parameters(method = "data")
	public void searchFtthProjectWithOneSearchCriteria() {
		BFLogger.logInfo("[Step 1] Open main page");
		MainPage mainPage = new MainPage();
		
		BFLogger.logInfo("[Step 2] Click navigation bar - FTTH Installationsprojekt Suche");
		mainPage.clickNavBarFtthInstallation();
		FtthSearchPage ftthSearch = mainPage.clickNavBarFtthProjectSuchen();
		
		BFLogger.logInfo("[Step 3] Fill search criteria");
		ftthSearch.setProjectName("Project-Name_3");
		
		BFLogger.logInfo("[Step 4] Click search");
		ftthSearch.clickSearch();

		
		BFLogger.logInfo("[Step 5] Verify if correct search results are loaded");
		String tableContent[][] = ftthSearch.getFtthProjectTableValues();
		ListElements results = ftthSearch.getResultList();
		assertTrue("# of results is not equal 1", results.getSize() == 1);
		assertEquals(tableContent[0][0], "Project-Name_3");
		
	}
		
		
	@Override
	public void setUp() {
		// TODO Auto-generated method stub
		
	}
	
	@Override
	public void tearDown() {
		// TODO Auto-generated method stub
		
	}
}
