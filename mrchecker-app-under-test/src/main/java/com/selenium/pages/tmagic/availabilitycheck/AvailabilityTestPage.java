package com.selenium.pages.tmagic.availabilitycheck;



import org.openqa.selenium.By;

import com.capgemini.mrchecker.selenium.core.BasePage;
import com.capgemini.mrchecker.selenium.core.newDrivers.INewWebDriver;
import com.capgemini.mrchecker.selenium.core.newDrivers.elementType.InputTextElement;
import com.capgemini.mrchecker.selenium.core.newDrivers.elementType.LabelElement;
import com.capgemini.mrchecker.test.core.logger.BFLogger;
import com.selenium.pages.tmagic.AvailabilityPage;

public class AvailabilityTestPage extends BasePage {
	
	private static final By selectorKlsId = By.id("searchCriteriaForm:inputboxForKlsId");
	private static final By selectorSearchButton = By.id("searchCriteriaForm:searchButton");
	private static final By selectorStatusOutput = By.id("searchResultForm:checkedklsIdStatus");
	private static final By selectorKlsIdOutput = By.id("searchResultForm:checkedKlsId");
	private static final By selectorPlannedStartOutput = By.id("searchResultForm:checkedklsIdBuildingStartTime");
	private static final By selectorPlannedEndOutput = By.id("searchResultForm:checkedklsIdBuildingEndTime");
	private static final By selectorReasonForNoConstructionOutput = By.id("searchResultForm:checkedklsIdNotBuildingReason");
	private static final By selectorTerminationErrorOutput = By.id("searchResultForm:checkedklsIdTerminationError");


	

	public AvailabilityTestPage(INewWebDriver driver, AvailabilityPage parent) {
		super(driver, parent);
	}
	@Override
	public boolean isLoaded() {
		if(getDriver().getTitle().equals(pageTitle())) {
			return true;
			}
			return false;
			}

	@Override
	public void load() {
	}

	@Override
	public String pageTitle() {
		return "Portal";
	}

	public void clickSearch() {
		getDriver().elementButton(selectorSearchButton)
				.click();
		BFLogger.logInfo("Clicking search on Availability Test Page");
		getDriver().waitForPageLoaded();
	}
	
	public void setKlsId(String text) {
		InputTextElement inputTextElement = new InputTextElement(selectorKlsId);
		inputTextElement.clearInputText();
		inputTextElement.setInputText(text);
	}
	
	public String getStatusOutput() {
		LabelElement outputTextElement = new LabelElement(selectorStatusOutput);
		return outputTextElement.getText();
	}

	public String getKlsIdOutput() {
		LabelElement outputTextElement = new LabelElement(selectorKlsIdOutput);
		return outputTextElement.getText();
	}

	public String getPlannedStartOutput() {
		LabelElement outputTextElement = new LabelElement(selectorPlannedStartOutput);
		return outputTextElement.getText();
	}

	public String getPlannedEndOutput() {
		LabelElement outputTextElement = new LabelElement(selectorPlannedEndOutput);
		return outputTextElement.getText();
	}

	public String getReasonForNoConstructionOutput() {
		LabelElement outputTextElement = new LabelElement(selectorReasonForNoConstructionOutput);
		return outputTextElement.getText();
	}

	public String getTerminationErrorOutput() {
		LabelElement outputTextElement = new LabelElement(selectorTerminationErrorOutput);
		return outputTextElement.getText();
	}

}
