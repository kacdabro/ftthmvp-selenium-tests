package com.selenium.pages.tmagic;

import org.openqa.selenium.By;

import com.capgemini.mrchecker.selenium.core.BasePage;
import com.capgemini.mrchecker.selenium.core.newDrivers.INewWebDriver;
import com.capgemini.mrchecker.test.core.logger.BFLogger;
import com.selenium.pages.environment.GetEnvironmentParam;
import com.selenium.pages.environment.PageSubURLsTmagicEnum;
import com.selenium.pages.environment.PageTitlesEnum;
import com.selenium.pages.tmagic.availabilitycheck.AvailabilityTestPage;
import com.selenium.pages.tmagic.ftthInstallation.FtthSearchPage;

public class AvailabilityPage extends BasePage {
	
	private static final By selectorNavBarFtthInstallation = By.xpath("//a[contains(text(),'FTTH Installation')]");
	private static final By selectorNavBarFtthProjektSuche = By.xpath("//a[contains(text(),'FTTH Projekt Suchen')]");
	private static final By selectorNavBarAvailability = By.xpath("//a[contains(text(),'Availability')]");
	private static final By selectorNavBarAvailabilityTestPage = By.xpath("//a[contains(text(),'FTTH Projekt Suchen')]");
	private static final String availabilityPageHost = "http://availability-check-test-ui:8080/";
		
	public AvailabilityPage() {
		this(getDriver(), null);
	}
	
	public AvailabilityPage(INewWebDriver driver, BasePage parent) {
		super(driver, parent);
	}
	
	private BasePage parent;
	
	@Override
	public void load() {
		getDriver().get(availabilityPageHost + PageSubURLsTmagicEnum.AVAILABILITY_TEST.getValue());
	}
	
	@Override
	public BasePage getParent() {
		return this.parent;
	}
	
	@Override
	public void setParent(BasePage parent) {
		// There is not parent
		this.parent = parent;
	}
	
	@Override
	public String pageTitle() {
		return PageTitlesEnum.MAIN_PAGE.toString();
	}
	
	@Override
	public boolean isLoaded() {
		return isUrlAndPageTitleAsCurrentPage(availabilityPageHost + PageSubURLsTmagicEnum.AVAILABILITY_TEST.getValue());
	}
	
	/*
	public void clickNavBarAvailability() {
		getDriver().elementButton(selectorNavBarAvailability)
				.click();
		BFLogger.logInfo("Clicking on Navigation bar menu item - Availability");
		getDriver().waitForPageLoaded();
	}
	*/
	
	public AvailabilityTestPage clickNavBarAvailabilityTestPage() {
		// getDriver().waitForPageLoaded();

		AvailabilityTestPage availabilityTestPage = new AvailabilityTestPage(getDriver(), this);
		// getDriver().elementButton(selectorNavBarAvailabilityTestPage)
		//		.click();
		BFLogger.logInfo("Clicking on Navigation bar link - Availability Test Page");
		return availabilityTestPage;
	}
	
	
}
