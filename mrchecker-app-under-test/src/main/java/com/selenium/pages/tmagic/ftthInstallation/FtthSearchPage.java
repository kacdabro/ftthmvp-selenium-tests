package com.selenium.pages.tmagic.ftthInstallation;



import org.openqa.selenium.By;

import com.capgemini.mrchecker.selenium.core.BasePage;
import com.capgemini.mrchecker.selenium.core.newDrivers.INewWebDriver;
import com.capgemini.mrchecker.selenium.core.newDrivers.elementType.InputTextElement;
import com.capgemini.mrchecker.selenium.core.newDrivers.elementType.ListElements;
import com.capgemini.mrchecker.test.core.logger.BFLogger;
import com.selenium.pages.tmagic.MainPage;

public class FtthSearchPage extends BasePage {
	
	private static final By selectorProjektname = By.id("searchCriteriaForm:projectName");
	private static final By selectorProjektStatus = By.id("searchCriteriaForm:projectStatus");
	private static final By selectorSearchButton = By.id("searchCriteriaForm:searchButton");
	private static final By selectorFtthProjectTable = By.xpath("//div[3]/table[1]");
	private static final By selectorFtthProjectTableRow = By.xpath("//div[3]/table[1]/tbody/tr");
	private static final By selectorFtthProjectTableColumn = By.xpath("//div[3]/table[1]/tbody/tr[1]/td");


	

	public FtthSearchPage(INewWebDriver driver, MainPage parent) {
		super(driver, parent);
	}
	@Override
	public boolean isLoaded() {
		if(getDriver().getTitle().equals(pageTitle())) {
			return true;
			}
			return false;
			}

	@Override
	public void load() {
	}

	@Override
	public String pageTitle() {
		return "Portal";
	}

	public void clickSearch() {
		getDriver().elementButton(selectorSearchButton)
				.click();
		BFLogger.logInfo("Clicking search on FTTH Installationsprojekt Suchen Page");
		getDriver().waitForPageLoaded();
	}
	
	public ListElements getResultList() {
		return getDriver().elementList(selectorFtthProjectTable);
	}
	
	public void setProjectName(String text) {
		InputTextElement inputTextElement = new InputTextElement(selectorProjektname);
		inputTextElement.clearInputText();
		inputTextElement.setInputText(text);
	}
	
	public void setProjectStatus(ProjectStatus projectstatus) {
		InputTextElement inputTextElement = new InputTextElement(selectorProjektStatus);
		inputTextElement.clearInputText();
		inputTextElement.setInputText(projectstatus.toString());
	}

	
	
	public String[][] getFtthProjectTableValues(){
		
		//divided xpath to tablecells
		String firstPart = "//div[3]/table/tbody/tr["; 
		String secondPart = "]/td[";
		String thirdPart = "]/span";
		
		//locate rows and columns
		ListElements rows = getDriver().elementList(selectorFtthProjectTableRow);
		ListElements columns = getDriver().elementList(selectorFtthProjectTableColumn);
				
		//get row and column count
		int rowCount = rows.getSize();
		int columnCount = 7;
		
		String tableContent[][] = new String[rowCount][columnCount];
		
		//Iterate through rows
		for(int i=1; i <= rowCount; i++) {
			
		//Iterate through columns (j=2 -> skipped first column)
			for (int j=2; j <= columnCount; j++) {
		//Read values from cells and assign to array
				tableContent[i-1][j-2] = getDriver().findElementDynamic(By.xpath(firstPart + i + secondPart + j + thirdPart)).getText();
				BFLogger.logInfo("Reading table: Row: "+i+" Column: "+j+" Value: "+tableContent[i-1][j-1]);
			}
		}
		return tableContent;
	}

}
