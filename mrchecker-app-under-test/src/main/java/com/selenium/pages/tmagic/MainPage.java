package com.selenium.pages.tmagic;

import org.openqa.selenium.By;

import com.capgemini.mrchecker.selenium.core.BasePage;
import com.capgemini.mrchecker.selenium.core.newDrivers.INewWebDriver;
import com.capgemini.mrchecker.test.core.logger.BFLogger;
import com.selenium.pages.environment.GetEnvironmentParam;
import com.selenium.pages.environment.PageSubURLsTmagicEnum;
import com.selenium.pages.environment.PageTitlesEnum;
import com.selenium.pages.tmagic.availabilitycheck.AvailabilityTestPage;
import com.selenium.pages.tmagic.ftthInstallation.FtthSearchPage;

public class MainPage extends BasePage {
	
	private static final By selectorNavBarFtthInstallation = By.xpath("//a[contains(text(),'FTTH Installation')]");
	private static final By selectorNavBarFtthProjektSuche = By.xpath("//a[contains(text(),'FTTH Projekt Suchen')]");
	private static final By selectorNavBarAvailability = By.xpath("//a[contains(text(),'Availability')]");
	private static final By selectorNavBarAvailabilityTestPage = By.xpath("//a[contains(text(),'FTTH Projekt Suchen')]");
		
	public MainPage() {
		this(getDriver(), null);
	}
	
	public MainPage(INewWebDriver driver, BasePage parent) {
		super(driver, parent);
	}
	
	private BasePage parent;
	
	@Override
	public void load() {
		getDriver().get(GetEnvironmentParam.WWW_FRONT_URL.getValue() + PageSubURLsTmagicEnum.FTTH_PROJECT_SEARCH.getValue());
	}
	
	@Override
	public BasePage getParent() {
		return this.parent;
	}
	
	@Override
	public void setParent(BasePage parent) {
		// There is not parent
		this.parent = parent;
	}
	
	@Override
	public String pageTitle() {
		return PageTitlesEnum.MAIN_PAGE.toString();
	}
	
	@Override
	public boolean isLoaded() {
		return isUrlAndPageTitleAsCurrentPage(GetEnvironmentParam.WWW_FRONT_URL.getValue() + PageSubURLsTmagicEnum.FTTH_PROJECT_SEARCH.getValue());
	}
	
	public void clickNavBarFtthInstallation() {
		getDriver().elementButton(selectorNavBarFtthInstallation)
				.click();
		BFLogger.logInfo("Clicking on Navigation bar menu item - FTTH Installation");
		getDriver().waitForPageLoaded();
	}
	
	public FtthSearchPage clickNavBarFtthProjectSuchen() {
		getDriver().waitForPageLoaded();

		FtthSearchPage ftthSearchPage = new FtthSearchPage(getDriver(), this);
		getDriver().elementButton(selectorNavBarFtthProjektSuche)
				.click();
		BFLogger.logInfo("Clicking on Navigation bar link - FTTH Projekt Suche");
		return ftthSearchPage;
	}
	
}
