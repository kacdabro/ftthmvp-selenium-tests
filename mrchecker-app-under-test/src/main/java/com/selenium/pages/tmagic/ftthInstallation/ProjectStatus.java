package com.selenium.pages.tmagic.ftthInstallation;

public enum ProjectStatus {
	OPEN("Open"),
	IN_PROGRESS("In progress"),
	CLOSED("Closed");
	
	
	private final String text;
	
	ProjectStatus(String projectstatus) {
		this.text = projectstatus;
	}
	
	public String toString() {
		return text;
	}
}
