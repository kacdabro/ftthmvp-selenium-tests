package com.selenium.pages.tmagic.mobile.gfTaInventarisation;


import org.openqa.selenium.By;

import com.capgemini.mrchecker.selenium.core.BasePage;
import com.capgemini.mrchecker.selenium.core.newDrivers.INewWebDriver;
import com.capgemini.mrchecker.selenium.core.newDrivers.elementType.InputTextElement;
import com.capgemini.mrchecker.selenium.core.newDrivers.elementType.ListElements;
import com.capgemini.mrchecker.test.core.logger.BFLogger;
import com.selenium.pages.environment.GetEnvironmentParam;
import com.selenium.pages.environment.PageSubURLsTmagicEnum;
import com.selenium.pages.tmagic.ftthInstallation.FtthSearchPage;

public class WorkorderSelectionPage extends BasePage {
	
	private static final By selectorFirstWorkoder = By.id("__item0-__component0---Master--woList-0");
	private static final By selectorSearchWorkorder = By.id("__field0-I");
	private static final By selectorModifyWorkorderButton = By.id("__action0-button-inner");
	private static final By selectorGfTaStreet = By.id("__text9");

	

	public WorkorderSelectionPage(INewWebDriver driver, com.selenium.pages.tmagic.MainPage mainPage) {
		super(driver, mainPage);
	}
	@Override
	public boolean isLoaded() {
		if(getDriver().getTitle().equals(pageTitle())) {
			return true;
			}
			return false;
			}

	@Override
	public void load() {
		getDriver().get(GetEnvironmentParam.MOBILE_CLIENT.getValue() + PageSubURLsTmagicEnum.MOBILE_WORKORDER.getValue());
	}

	@Override
	public String pageTitle() {
		return "Portal";
	}

	public void clickFirstWorkorder() {
		getDriver().waitForPageLoaded();
		getDriver().elementButton(selectorFirstWorkoder)
				.click();
		BFLogger.logInfo("Clicking first Workorder in the list");
		getDriver().waitForPageLoaded();
	}
	
//	public ListElements getResultList() {
//		return getDriver().elementList(selectorResultTable);
//	}
	
	public void setSearchWorkorder(String workorderID) {
		InputTextElement inputTextElement = new InputTextElement(selectorSearchWorkorder);
		inputTextElement.clearInputText();
		inputTextElement.setInputText(workorderID);
	}
	
	public GfTaMontagePage clickModifyWorkorder() {
		GfTaMontagePage modifyWorkorderPage = new GfTaMontagePage(getDriver(), this);
		getDriver().elementButton(selectorModifyWorkorderButton)
				.click();
		BFLogger.logInfo("Clicking on Navigation bar link - FTTH Projekt Suche");
		getDriver().waitForPageLoaded();
		return modifyWorkorderPage;
	}
	
	public String getGfTaStreet() {
		String streetName = getDriver().elementLabel(selectorGfTaStreet).toString();
		return streetName;
	}
	

}
