package com.selenium.pages.tmagic.mobile.gfTaInventarisation;

import org.openqa.selenium.By;

import com.capgemini.mrchecker.selenium.core.BasePage;
import com.capgemini.mrchecker.selenium.core.newDrivers.INewWebDriver;

public class GfTaMontagePage extends BasePage{

	private static final By selectorCorningRadioButton = By.cssSelector("#__button1-label");
	private static final By selectorGfTaStreetHeader = By.cssSelector("#__text27");
	private static final By selectorGfApBezHeader = By.cssSelector("#__text28");

	@Override
	public boolean isLoaded() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public void load() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public String pageTitle() {
		// TODO Auto-generated method stub
		return null;
	}
	
	public GfTaMontagePage(INewWebDriver driver, com.selenium.pages.tmagic.mobile.gfTaInventarisation.WorkorderSelectionPage workorderSelectionPage) {
		super(driver, workorderSelectionPage);
	}
	
	public String getAdressFromHeader() {
		getDriver().waitForElement(selectorGfTaStreetHeader);
		String adress = getDriver().elementLabel(selectorGfTaStreetHeader).toString();
		return adress;
	}
	
	public String getGFAPBezeichnerFromHeader() {
		getDriver().waitForElement(selectorGfApBezHeader);
		String adress = getDriver().elementLabel(selectorGfApBezHeader).toString();
		return adress;
	}
	
	public void selectGFTARadioButton() {
		getDriver().waitForElement(selectorCorningRadioButton);
		getDriver().elementButton(selectorCorningRadioButton).click();	
	}
	


}
