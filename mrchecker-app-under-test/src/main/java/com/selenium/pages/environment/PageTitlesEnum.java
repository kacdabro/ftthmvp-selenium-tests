package com.selenium.pages.environment;

public enum PageTitlesEnum {
	LOGIN_PAGE("Portal"),
	FTTH_PROJECT_SEARCH("Portal"),
	FRAMEANDWINDOWS("Portal"),
	MAIN_PAGE("Portal"),
	CREATE_USERNAME_PAGE(""),
	MAIN_PAGE_CLASSIC(""),
	FULL_VIEW_TAB(""),;
	
	private String value;
	
	private PageTitlesEnum(String value) {
		this.value = value;
	}
	
	@Override
	public String toString() {
		return value;
	}
}