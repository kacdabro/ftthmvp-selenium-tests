package com.selenium.pages.environment;

public enum PageSubURLsTmagicEnum {
	
	FTTH_PROJECT_SEARCH("ftth-installation-ui/ftthinstalationproject/search"),
	GFTA_SEARCH("ne3ne4-inventory-ui/gfta/search"),
	MOBILE_WORKORDER("fibrewo2/#/tasklist"),
	FAQ("content/apps/static/faqpopup/index.html"),
	AUTOMATION_PRACTICE_FORM("automation-practice-form"),
	TABS("tabs/"),
	TOOLTIP("tooltip/"),
	MENU("menu/"),
	AVAILABILITY_TEST("availability-check-test-ui/availabilitychecktest/check");
	
	/*
	 * Sub urls are used as real locations in Bank test environment
	 */
	private String subURL;
	
	private PageSubURLsTmagicEnum(String subURL) {
		this.subURL = subURL;
	};
	
	private PageSubURLsTmagicEnum() {
		
	}
	
	@Override
	public String toString() {
		return getValue();
	}
	
	public String getValue() {
		return subURL;
	}
	
}